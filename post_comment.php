<?php 
	include_once 'helpers/Format.php';
	spl_autoload_register(function($class_name) {
		include_once 'classess/'.$class_name.'.php';
	});
	$com = new Comment();
	$fm  = new Format(); 
 ?>
<?php
	if(isset($_POST['submit'])){
		$name    = $_POST['name'];
		$body = $_POST['body'];
		$insert_comment = $com->insertComment($name, $body);
		if (isset($insert_comment)) {
			header("location: index.php?msg=".urlencode('Comment Posted Successfully...'));
		}
	}
?>
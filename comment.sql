-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 11, 2017 at 04:56 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `comment`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `body` text NOT NULL,
  `comment_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `name`, `body`, `comment_time`) VALUES
(1, 'Rasel', 'Hello! Have a nice day.', '2016-09-29 20:26:58'),
(2, 'Alex', 'Thank you very much', '2016-09-29 20:27:43'),
(3, 'Roy', 'very good', '2016-09-29 20:29:08'),
(4, 'Joy', 'How are you Roy?', '2016-09-29 20:47:17'),
(5, 'Roy', 'I am fine. And you?', '2016-09-29 20:49:18'),
(6, 'Joy', 'fine', '2016-09-29 20:49:35'),
(7, 'Rasel', 'Hello Joy, What are you doing now? Congratulation for your brilliant success. May Allah live long. Thank you so much. ', '2016-10-02 21:41:38'),
(8, 'Joy', 'I am very fine.', '2016-10-02 00:00:00'),
(9, 'Rasel', 'Nice to meet you', '2016-10-02 22:22:45'),
(10, 'Sumon', 'Thank you very much', '2016-10-02 22:25:28'),
(11, 'Jahir', 'I am very glad to hearing this news', '2016-10-03 09:41:26'),
(14, 'Eva', ' I am very glad to hearing this news.  I am very glad to hearing this news.  I am very glad to hearing this news.', '2016-10-16 09:03:07'),
(16, 'Jibon', 'Hello ', '2016-11-27 22:44:25'),
(19, 'Akash', 'I am very happy', '2016-12-15 15:47:11'),
(21, 'Abu Sufian', 'Hello Guys How are u?', '2016-12-15 15:55:11'),
(22, 'SM Rasel', 'Hello ', '2016-12-15 23:52:28'),
(23, 'Sofian', 'Hello how are you today?', '2016-12-22 08:22:42'),
(24, 'Rasel', 'Hi', '2017-02-11 09:38:13'),
(25, 'Jibon', 'Hello', '2017-02-11 09:43:06'),
(26, 'Akash', 'Hello', '2017-02-11 09:44:04'),
(27, 'Ariful Islam', 'I''m fine what about you bro?', '2017-02-11 09:46:56'),
(28, 'Jason Roy', 'Hello Sir', '2017-02-11 09:54:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

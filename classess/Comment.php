<?php 
	include_once 'lib/DB.php';
	include_once 'helpers/Format.php';
	class Comment{
		public $db;
		public function __construct() {
			$this->db = new DB();
			$this->fm = new Format();
		}

		public function getAllComment() {
			$query  = "SELECT * FROM comments ORDER BY id DESC";
			$result = $this->db->select($query);
			return $result;
		}

		public function insertComment($name, $body) {
			$name = $this->fm->validation($name);
			$body = $this->fm->validation($body);
			$name = mysqli_real_escape_string($this->db->link, $name);
			$body = mysqli_real_escape_string($this->db->link, $body);
			$query = "INSERT INTO comments(name, body, comment_time) VALUES('$name', '$body', now())";
			$insertdata = $this->db->create($query);
			if ($insertdata) {
				return $insertdata;
			}
		}
	}
 ?>
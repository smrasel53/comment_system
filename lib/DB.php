<?php 
	include_once 'config/config.php';
	class DB{
		public $host   = DB_HOST;
		public $user   = DB_USER;
		public $pass   = DB_PASS;
		public $dbname = DB_NAME;

		public $link;
		public $error;

		public function __construct() {
			$this->connectDB();
		}

		private function connectDB() {
			$this->link = new mysqli($this->host, $this->user, $this->pass, $this->dbname);
			if (!$this->link) {
				$this->error = "Connection fail".$this->link->connect_error;
				return false;
			}
		}

		// Select or Read Data
		public function select($query) {
			$result = $this->link->query($query) or die($this->link->error.__LINE__);
			if ($result->num_rows > 0) {
				return $result;			
			} else {
				return false;
			}
		}

		// Create Data
		public function create($query) {
			$create_row = $this->link->query($query) or die($this->link->error.__LINE__);
			if ($create_row) {
				return $create_row;
			} else {
				return false;
			}
		}

		
	}
 ?>
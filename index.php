<?php 
	include_once 'helpers/Format.php';
	spl_autoload_register(function($class_name) {
		include_once 'classess/'.$class_name.'.php';
	});
	$com = new Comment();
	$fm  = new Format(); 
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Comment System</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<div class="wrapper clear">
		<header class="headeropt">
			<h2>Comment System</h2>
		</header>
		<section class="content clear">
			<div class="box">
				<ul>
				<?php
					$result = $com->getAllComment();
					if ($result) {
						foreach ($result as $comment) {
							?>
							<li><span><?php echo $fm->formatDate($comment['comment_time']); ?></span> - <b><?php echo $comment['name']; ?></b> <?php echo $comment['body']; ?>
							</li>
							<?php
						}
					} else {
						?>
						<li><b><?php echo "No Comment Avaiable."; ?></b></li>
						<?php
					}
				?>
				</ul>
			</div>
			<div class="shoutform clear">
				<?php 
					if (isset($_GET['msg'])) {
						$msg = $_GET['msg'];
						echo "<span style='color:green'>".$msg."</span>";
					}
				 ?>
				<form action="post_comment.php" method="post">
					<table>
						<tr>
							<td>Name</td>
							<td>:</td>
							<td><input type="text" name="name" placeholder="Please enter your name"  required="1"></td>
						</tr>
						<tr>
							<td>Body</td>
							<td>:</td>
							<td><input type="text" name="body" placeholder="Please enter your text" required="1"></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td><input type="submit" name="submit" value="Comment"></td>
						</tr>
					
					</table>
				</form>
			</div>
		</section>
		<footer class="footeropt clear">
			<p>Copyright &copy; Comment System | Developed By <a href="www.facebook.com/smrasel53" target="_blank">Abu Sofian Rasel</a></p>
		</footer>
	</div>
</body>
</html>




